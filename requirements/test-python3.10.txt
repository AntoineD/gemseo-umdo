#
# This file is autogenerated by pip-compile with Python 3.10
# by the following command:
#
#    pip-compile --extra=dev --extra=test --output-file=requirements/test-python3.10.txt
#
certifi==2023.7.22
    # via requests
charset-normalizer==3.2.0
    # via requests
contourpy==1.1.1
    # via matplotlib
covdefaults==2.3.0
    # via gemseo-umdo (pyproject.toml)
coverage[toml]==7.3.1
    # via
    #   covdefaults
    #   pytest-cov
cycler==0.12.0
    # via matplotlib
dill==0.3.7
    # via openturns
docstring-inheritance==2.0.0
    # via gemseo
et-xmlfile==1.1.0
    # via openpyxl
exceptiongroup==1.1.3
    # via pytest
execnet==2.0.2
    # via pytest-xdist
fastjsonschema==2.18.0
    # via gemseo
fonttools==4.43.0
    # via matplotlib
gemseo[all] @ git+https://gitlab.com/gemseo/dev/gemseo.git@develop
    # via gemseo-umdo (pyproject.toml)
genson==1.2.2
    # via gemseo
graphviz==0.20.1
    # via gemseo
h5py==3.9.0
    # via gemseo
idna==3.4
    # via requests
iniconfig==2.0.0
    # via pytest
jinja2==3.1.2
    # via gemseo
joblib==1.3.2
    # via scikit-learn
kiwisolver==1.4.5
    # via matplotlib
markupsafe==2.1.3
    # via jinja2
matplotlib==3.8.0
    # via
    #   gemseo
    #   gemseo-umdo (pyproject.toml)
mpmath==1.3.0
    # via sympy
networkx==3.1
    # via gemseo
nlopt==2.7.1
    # via gemseo
numpy==1.26.0
    # via
    #   contourpy
    #   gemseo
    #   gemseo-umdo (pyproject.toml)
    #   h5py
    #   matplotlib
    #   nlopt
    #   pandas
    #   pydoe2
    #   pyxdsm
    #   scikit-learn
    #   scipy
openpyxl==3.1.2
    # via gemseo
openturns==1.21
    # via gemseo
packaging==23.1
    # via
    #   gemseo
    #   matplotlib
    #   pytest
pandas==2.1.1
    # via gemseo
pillow==10.0.0
    # via
    #   gemseo
    #   matplotlib
pluggy==1.3.0
    # via pytest
psutil==5.9.5
    # via openturns
pydantic==1.10.13
    # via gemseo
pydoe2==1.3.0
    # via gemseo
pyparsing==3.1.1
    # via matplotlib
pytest==7.4.2
    # via
    #   gemseo-umdo (pyproject.toml)
    #   pytest-cov
    #   pytest-xdist
pytest-cov==4.1.0
    # via gemseo-umdo (pyproject.toml)
pytest-xdist==3.3.1
    # via gemseo-umdo (pyproject.toml)
python-dateutil==2.8.2
    # via
    #   matplotlib
    #   pandas
pytz==2023.3.post1
    # via pandas
pyxdsm==2.3.0
    # via gemseo
requests==2.31.0
    # via gemseo
scikit-learn==1.3.1
    # via gemseo
scipy==1.10.1
    # via
    #   gemseo
    #   gemseo-umdo (pyproject.toml)
    #   pydoe2
    #   scikit-learn
six==1.16.0
    # via python-dateutil
strenum==0.4.15
    # via gemseo
sympy==1.12
    # via gemseo
threadpoolctl==3.2.0
    # via scikit-learn
tomli==2.0.1
    # via
    #   coverage
    #   pytest
tqdm==4.66.1
    # via gemseo
typing-extensions==4.8.0
    # via
    #   gemseo
    #   pydantic
tzdata==2023.3
    # via pandas
urllib3==2.0.5
    # via requests
xdsmjs==2.0.0
    # via gemseo
xxhash==3.3.0
    # via gemseo
